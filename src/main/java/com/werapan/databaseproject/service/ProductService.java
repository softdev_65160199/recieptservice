/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author sarit
 */
public class ProductService {
    public Product getById(int id){
        ProductDao product = new ProductDao();
        return product.get(id);
    }
    public List<Product> getProducts(){
        ProductDao product = new ProductDao();
        return product.getAll(" product_id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao product = new ProductDao();
        return product.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao product = new ProductDao();
        return product.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao product = new ProductDao();
        return product.delete(editedProduct);
    }
}
